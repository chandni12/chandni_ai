
from flask import Blueprint, request, jsonify


from api.utils import api_checker


aiTest = Blueprint("aiTest", __name__, url_prefix="/aiTest")


@aiTest.route('/', methods=["OPTIONS", "GET"])
def home():
    return "Home of aiprofile ", 200
